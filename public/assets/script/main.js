(function(window, document){

    var camera;
    var source;
    var sceneMain;
    var sceneButton;
    var sceneInteraction;
    var content;

    var lon = 0;
    var lat = 0;
    var phi = 0;
    var theta = 0;
    var distance = 500;
    var onMouseDownLon = 0;
    var onMouseDownLat = 0;
    var onPointerDownLat = 0;
    var onPointerDownLon = 0;
    var onMouseDownMouseX = 0;
    var onMouseDownMouseY = 0;
    var isUserInteracting = false;
    var onPointerDownPointerX = 0;
    var onPointerDownPointerY = 0;

    var fov = 90;
    var near = 1;
    var far = 1000;
    var maxAngleX = 60;
    var maxAngleY = 60;

    window.addEventListener('load', init);

    function init(){
        camera = new THREE.PerspectiveCamera(fov, window.innerWidth / window.innerHeight, near, far);
        camera.target = new THREE.Vector3(0, 0, 0);
        createSource();

        window.addEventListener('resize', updateScene);
    }

    function createSource(){
        source = document.createElement(/mp4/gi.test(DATACONFIG.src) ? 'video' : 'img');
        source.src = DATACONFIG.src;
        source.addEventListener(/video/gi.test(source.tagName) ? 'canplay' : 'load', createScene);
        source.setAttribute('style', 'position:absolute; opacity:0; pointer-events:none;');
        document.querySelector('body').appendChild(source);
    }

    function createScene(e){
        e.target.removeEventListener(e.type, arguments.callee);
        sceneMain = new MainScene(source);
        sceneButton = new ButtonScene();
        sceneInteraction = new InteractionScene();
        controllersScene();
        updateCamera();

    }

    function controllersScene(){
        var globe = sceneInteraction.getDom();

        globe.addEventListener('mousemove', controllerMove);
        globe.addEventListener('mousedown', clickMoveDown);
        globe.addEventListener('mouseup', clickMoveUp);

        try{
            document.createEvent("TouchEvent");
            globe.addEventListener("touchmove", controllerMove);
            globe.addEventListener("touchstart", clickMoveDown);
            globe.addEventListener("touchend", clickMoveUp);
        }catch(e){}

        window.addEventListener("deviceorientation", getOrientation);
    }

    function getOrientation(e){
        if(e){
            /*lon = ( onPointerDownPointerX - (e.alpha + 180) ) * 0.1 + onPointerDownLon;
            lat = ( (e.beta + 180) - onPointerDownPointerY ) * 0.1 + onPointerDownLat;*/

            lon = e.alpha * -1;
            lat = 90 - e.beta;
        }
    }

    function clickMoveDown(e){
        e.preventDefault();
        
        isUserInteracting = true;

        onPointerDownPointerX =  e.type == "touchstart" ? (e.targetTouches[0] || e.originalEvent.changedTouches[0]).clientX : e.clientX;
        onPointerDownPointerY = e.type == "touchstart" ? (e.targetTouches[0] || e.originalEvent.changedTouches[0]).clientY : e.clientY;

        getPosition(onPointerDownPointerX, onPointerDownPointerY);

        onPointerDownLon = lon;
        onPointerDownLat = lat;
    }

    function clickMoveUp(event){
        event.preventDefault();
        isUserInteracting = false;
    }

    function controllerMove(e){
        var x = e.type == "touchmove" ? (e.targetTouches[0] || e.originalEvent.changedTouches[0]).clientX : e.clientX;
        var y = e.type == "touchmove" ? (e.targetTouches[0] || e.originalEvent.changedTouches[0]).clientY : e.clientY;

        if(isUserInteracting){
            lon = ( onPointerDownPointerX - x ) * 0.1 + onPointerDownLon;
            lat = ( y - onPointerDownPointerY ) * 0.1 + onPointerDownLat;
        }
    }

    function getPosition(x, y){
       
       x -= sceneInteraction.getDom().offsetLeft;
       y -= sceneInteraction.getDom().offsetTop;
        
        var canvas = document.createElement('canvas');
        canvas.width = sceneInteraction.getDom().offsetWidth;
        canvas.height = sceneInteraction.getDom().offsetHeight;
        var context = canvas.getContext('2d');

        var image =  new Image();
        image.src =sceneInteraction.getDom().toDataURL();
        image.addEventListener('load', function(){
            context.drawImage(image, 0, 0);
            verifyColorContent(context.getImageData(x, y, 1, 1).data);
        });

    }

    function verifyColorContent(color){
        var r = Utils.rgbToHex(color[0]);
        var g = Utils.rgbToHex(color[1]);
        var b = Utils.rgbToHex(color[2]);
        var hex = r+g+b;

        if(color[3] > 0 && DATACONFIG.content[hex])
            console.log(DATACONFIG.content[hex].content);
        else
            console.log('Cor Não encontrada', color, hex);
    }

   

    function updateScene(){
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        sceneMain.updateScene();
    }

    function updateCamera(){
        lat = Math.max( - maxAngleX, Math.min( maxAngleY, lat ) );
        phi = THREE.Math.degToRad( 90 - lat );
        theta = THREE.Math.degToRad( lon );

        camera.position.x = distance * Math.sin( phi ) * Math.cos( theta );
        camera.position.y = distance * Math.cos( phi );
        camera.position.z = distance * Math.sin( phi ) * Math.sin( theta );

        camera.lookAt( camera.target );

        camera.position.copy( camera.target ).negate();
        
        sceneInteraction.update(camera);
        sceneMain.update(camera);
        sceneButton.update(camera);

        requestAnimationFrame(updateCamera);
    }

})(window, document);

var MainScene = (function(source){

    var scene;
    var render;
    var geometry;
    var sphere;
    var texture;

    function MainScene(){

        if(/video/gi.test(source.tagName))
            textureVideo();
        else
            textureImage();
        
        createScene();
        createRender();
        createSphere();

        document.querySelector('body').appendChild(render.domElement);

        render.domElement.classList.add('main-scene');
    }

    function textureImage(){
        var canvas = document.createElement('canvas');
        canvas.width = source.offsetWidth;
        canvas.height = source.offsetHeight;
        canvas.getContext('2d').drawImage(source, 0, 0);

        texture = new THREE.CanvasTexture(canvas);
    }

    function textureVideo(){
        texture = new THREE.VideoTexture(source);
        texture.minFilter = THREE.NearestFilter;
        texture.maxFilter = THREE.NearestFilter;
        texture.format = THREE.RGBFormat;
        texture.generateMipmaps = false;
        source.setAttribute('loop', true);
        source.setAttribute('autoplay', true);
        source.play();
    }

    function createScene(){
        scene = new THREE.Scene();
    }

    function createRender(){
        render = new THREE.WebGLRenderer({preserveDrawingBuffer: true});
        render.setSize(window.innerWidth, window.innerHeight);
        render.setClearColor(0xffffff, 1);
    }

    function createSphere(){
        geometry = new THREE.SphereBufferGeometry(source.offsetWidth * 0.5, 50, 50);
        geometry.scale(-1, 1, 1);
        var material = new THREE.MeshBasicMaterial({map: texture});
        sphere = new THREE.Mesh(geometry, material);

        scene.add(sphere);
    }

    this.updateScene = function(){
        render.setSize(window.innerWidth, window.innerHeight);
    }

    this.update = function(camera){
        render.render(scene, camera);
    }

    MainScene.apply(this, arguments);

    return this;
});

var ButtonScene = (function(){
    var scene;
    var render;
    var geometry;
    var sphere;
    var texture;

    function ButtonScene(){

        scene = new THREE.Scene();

        render = new THREE.WebGLRenderer({preserveDrawingBuffer: true, alpha: true});
        render.setSize(window.innerWidth , window.innerHeight);
        render.setClearColor(0xffffff, 0);

        texture = new THREE.TextureLoader().load(DATACONFIG.button, buttonLoad);
    }


    function buttonLoad(){
        geometry = new THREE.SphereBufferGeometry(texture.image.width * 0.5, 50, 50);
        geometry.scale(-1, 1, 1);
        var material = new THREE.MeshBasicMaterial({map: texture});
        sphere = new THREE.Mesh(geometry, material);

        scene.add(sphere);

        document.querySelector('body').appendChild(render.domElement);

        render.domElement.classList.add('buttons');
    }

    this.updateScene = function(){
        render.setSize(window.innerWidth, window.innerHeight);
    }

    this.update = function(camera){
        render.render(scene, camera);
    }

    this.getDom = function(){
        return render.domElement;
    }

    ButtonScene.apply(this, arguments);

    return this;
});

var InteractionScene = (function(){

    var scene;
    var render;
    var geometry;
    var sphere;
    var texture;

    function InteractionScene(){

        scene = new THREE.Scene();

        render = new THREE.WebGLRenderer({preserveDrawingBuffer: true, alpha: true});
        render.setSize(window.innerWidth , window.innerHeight);
        render.setClearColor(0xffffff, 0);

        texture = new THREE.TextureLoader().load(DATACONFIG.hint, interactionLoad);
    }


    function interactionLoad(){
        geometry = new THREE.SphereBufferGeometry(texture.image.width * 0.5, 50, 50);
        geometry.scale(-1, 1, 1);
        var material = new THREE.MeshBasicMaterial({map: texture});
        sphere = new THREE.Mesh(geometry, material);

        scene.add(sphere);

        document.querySelector('body').appendChild(render.domElement);

        render.domElement.classList.add('interactions');

    }

    this.updateScene = function(){
        render.setSize(window.innerWidth, window.innerHeight);
    }

    this.update = function(camera){
        render.render(scene, camera);
    }

    this.getDom = function(){
        return render.domElement;
    }

    InteractionScene.apply(this, arguments);

    return this;
});

var Utils = {
       
    rgbToHex: (function(rgb){
        var hex = Number(rgb).toString(16);
        hex = hex.length < 2 ? '0' + hex : hex;
        return hex;
    })
};